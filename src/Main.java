import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) throws FileNotFoundException {
        Game game = new Game();
        Player player1 = new Player("player1", 0, "", false);
        Player player2 = new Player("player2", 0, "", false);

        player1.giveCity();
        player2.giveCity();
        player1.setAnswerCorrect(player1.compareAnswersToTheList(player1.getAnswer()));
        player2.setAnswerCorrect(player2.compareAnswersToTheList(player2.getAnswer()));
        System.out.println(player1);
        System.out.println(player2);
        setPoints(player1,player2);
        System.out.println(player1);
        System.out.println(player2);
    }

    public static void setPoints(Player player1, Player player2) {
        if (player1.setAnswerCorrect() && player2.setAnswerCorrect()) {

            if (player1.getAnswer().equals(player2.getAnswer())) {
                player1.setPoints(player1.getPoints() + 10);
                player2.setPoints(player2.getPoints() + 10);
            } else if (!player1.getAnswer().equals(player2.getAnswer())) {
                player1.setPoints(player1.getPoints() + 15);
                player2.setPoints(player2.getPoints() + 15);
            }
        } else if (!player1.isAnswerCorrect()) player2.setPoints(player2.getPoints() + 20);
        else if (!player2.isAnswerCorrect()) player1.setPoints(player1.getPoints() + 20);
    }

}
