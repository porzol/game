package pl.sda.zadania;

public class Customer {

    private String name;
    private int age;
    private boolean pregnant;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isPregnant() {
        return pregnant;
    }

    public void setPregnant(boolean pregnant) {
        this.pregnant = pregnant;
    }

    public Customer(String name, int age, boolean pregnant) {

        this.name = name;
        this.age = age;
        this.pregnant = pregnant;
    }
}
